package com.company;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class Main {

    public static void main(String[] args) {
//        System.out.println(equalWords("geel", "geel"));
//        System.out.println(equalWords("", ""));
//        System.out.println(equalWords("gel", "geel"));

 //       System.out.println(matchMinOne("No I am your father", "? I am your father"));
    //    System.out.println(matchMinOne("Do Or do not There is no try", "Do Or do not ? There is no try"));

       // System.out.println(matchMinOne("The force will be with you", "The ? will be with ?"));

       System.out.println(matchMinOne("The Force will be with you", "The ? will be with ?"));
        System.out.println("data:");
        System.out.println(data);

    }

    static boolean equalWords(String woord1, String woord2) {
        if (woord1 == null && woord2 == null){
            return true;
        } else if (woord1.isEmpty() && woord2.isEmpty())
        {
            return true;
        }else{
            return woord1.charAt(0) == woord2.charAt(0) ? equalWords(woord1.substring(1), woord2.substring(1)) : false;
        }
    }

    static boolean equalSentence(String zin1, String zin2) {
        if (zin1 == null && zin2 == null) {
            return true;
        } else if (zin1.isEmpty() && zin2.isEmpty()){
            return true;
        }

        int i1 = zin1.indexOf(' ');
        int i2 = zin2.indexOf(' ');

        if (i1 == -1 && i2 == -1) {
            return equalWords(zin1, zin2);
        }else if(i1 != i2){
            return false;
        }else {
            String word1 = zin1.substring(0, i1);
            String zin1Rest = zin1.substring(i1 + 1);
            String word2 = zin2.substring(0, i2);
            String zin2Rest = zin2.substring(i2 + 1);

            if (equalWords(word1, word2)) {
                return equalSentence(zin1Rest, zin2Rest);
            } else {
                return false;
            }
        }
    }

    public static int teller = 0;
    public static Queue<String> data = new LinkedList<>();

    static public boolean matchMinOne(String zin1, String zin2) {
        //log
        teller++;
        System.out.println("matchMinOne oproepnr " + teller + " " + zin1 + " <-> " + zin2);
        data.add(" \" " + zin1 + " \" " + " matched met " + " \" " + zin2 + " \" ");

        if(zin1 == null && zin2 == null) return true;

        int i1 = zin1.indexOf(' ');
        int i2 = zin2.indexOf(' ');

        //i = -1 betekent laatste woord
        if( i1 == -1 && i2 == -1){
            //laatste woord bij beide dus indien vraagteken is het sowieso true
            if (zin2.equals("?")) return true;
            //indien geen vraagteken, controleer de woorden
            return equalWords(zin1, zin2);

        } else if(i1 == -1 || i2 == -1){
            data.remove(zin1 + " matched met " + zin2);
            return false;
        }else{
            String word1 = zin1.substring(0, i1);
            String zin1Rest = zin1.substring(i1 + 1);
            String word2 = zin2.substring(0, i2);
            String zin2Rest = zin2.substring(i2 + 1);

            if (word2.equals("?")) {
                if(matchMinOne(zin1, zin2Rest)){
                    return matchMinOne(zin1, zin2Rest);
                } else{
                    System.out.println("stop");
                    data.remove(zin1 + " matched met " + zin2);
                    return matchMinOne(zin1Rest, zin2Rest);
                }
            }else if (equalWords(word1, word2)) {
                return matchMinOne(zin1Rest, zin2Rest);
            } else {
                data.remove(zin1 + " matched met " + zin2);
                return false;
            }
        }
    }

    public static Map<String, String> woorden = new HashMap<String, String>();

    static public boolean matchMany(String zin1, String zin2){
        if (zin1 == null && zin2 == null) {
            return true;
        } else if (zin1.isEmpty() && zin2.isEmpty()){
            return true;
        }

        int i1 = zin1.indexOf(' ');
        int i2 = zin2.indexOf(' ');

        //indien het bij beide zinnen het laatste woord is
        if(i1 == -1 && i2 == -1){
            if(zin2.contains("*")){
                return true;
            } else if(equalWords(zin1, zin2)){
                return true;
            } else{
                return false;
            }
        } else if(i1 == -1){
            //code als eerste zin maar 1 woord meer heeft
        } else if(i2 == -1){
            //code als tweede zin maar 1 woord meer heeft
        } else{
            String word1 = zin1.substring(0, i1);
            String zin1Rest = zin1.substring(i1 + 1);
            String word2 = zin2.substring(0, i2);
            String zin2Rest = zin2.substring(i2 + 1);

            //als het patroon voorkomt
            if(word2.contains("*")){
                if(matchMany(zin1, zin2Rest)){
                    //code als het sterretje niets vervangt
                }else if(matchMany(zin1Rest, zin2Rest)){
                    //code als het sterretje 1 woord vervangt
                    //kijken of nog woorden vervangt
                }else{
                    return false;
                }
            }

            if(equalWords(word1, word2)){
                return matchMany(zin1Rest, zin2Rest);
            }
        }
         return false;
    }
}


