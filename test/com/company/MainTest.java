package com.company;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Recursie : MatcherDonderdagTest
 *
 * @author katja.verbeeck
 * @version 19/02/2021
 */
class MainTest {

    Main match;

    @BeforeEach
    void setup(){
        match = new Main();
    }

    @Test
    void equalWordsTest() {
        assertTrue(match.equalWords("word","word"));
        assertFalse(match.equalWords("word","woord"));
        assertTrue(match.equalWords("anaconda",new String("anaconda")));
        assertTrue(match.equalWords("",""));
        assertTrue(match.equalWords(null,null));
    }

    @Test
    void equalSentence(){
        assertTrue(match.equalSentence("No I am your father","No I am your father"));
        assertFalse(match.equalSentence("Do Or do not There is no try","Do Or do not There is try"));
        assertTrue(match.equalSentence("",""));
        assertTrue(match.equalSentence(null,null));
    }

    @Test
    void matchMinOneTest(){
        assertTrue(match.matchMinOne("No I am your father", "? I am your father"));
        assertTrue(match.matchMinOne("Do Or do not There is no try", "Do Or do not ? There is no try"));
        assertTrue(match.matchMinOne("The Force will be with you", "The ? will be with ?"));
        assertTrue(match.matchMinOne("",""));
        assertTrue(match.matchMinOne(null,null));
        assertTrue(match.matchMinOne("No I am your father", "No I ? your ?"));
        assertFalse(match.matchMinOne("Do Or do not There is try", "Do Or do not ? ?"));
    }

//    @Test
//    void matchManyTest(){
//        assertTrue(match.matchMany("No I am your father", "* I am your father"));
//        assertTrue(match.matchMany("Do Or do not There is no try", "Do Or do not * There is no try"));
//        assertTrue(match.matchMany("",""));
//        assertTrue(match.matchMany(null,null));
//        assertTrue(match.matchMany("No I am your father", "No * your * father *"));
//        assertTrue(match.matchMany("Do Or do not There is try", "Do Or do not *"));
//        assertFalse(match.matchMany("Do Or do not", "Do Or do not * there is try"));
//        assertTrue(match.matchMany("Do Or do not", "Do Or do not *"));
//        assertTrue(match.matchMany("The Force will be with you", "The * will be *"));
//    }
//
//    @Test
//    void matchTest(){
//        assertTrue(match.match("No I am your father", "No ? am *"));
//        assertFalse(match.match("I'm just a simple man trying to make my way in the universe","I'm just ? trying to make my way *"));
//        assertTrue(match.match("I'm just a simple man trying to make my way in the universe","I'm just * trying to make my way in the universe ?"));
//        assertTrue(match.match("Do Or do not There is no try", "Do Or do not ? * ?"));
//        assertTrue(match.match("",""));
//        assertTrue(match.match(null,null));
//        assertTrue(match.match("No I am your father", "No ? ? ? ? *"));
//        assertFalse(match.match("No I am your father", "No ? ? ?"));
//        assertTrue(match.match("No I am your father", "No ? am *"));
//
//
//    }

}